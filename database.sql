DROP VIEW IF EXISTS `excel`.`DokumentFirmaView`;
DROP TABLE IF EXISTS `excel`.`Firmy_Adresy` ;
DROP TABLE IF EXISTS `excel`.`Firmy_Uzivatele` ;
DROP TABLE IF EXISTS `excel`.`VerzeFirmy` ;
DROP TABLE IF EXISTS `excel`.`VerzeUzivatele` ;
DROP TABLE IF EXISTS `excel`.`Dokument` ;
DROP TABLE IF EXISTS `excel`.`Firma` ;
DROP TABLE IF EXISTS `excel`.`Uzivatel` ;
DROP TABLE IF EXISTS `excel`.`Rocnik` ;
DROP TABLE IF EXISTS `excel`.`Adresy` ;

-- -----------------------------------------------------
-- Table `excel`.`Uzivatel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`Uzivatel` (
  `id_uzivatel` INT(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `heslo` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  PRIMARY KEY (`id_uzivatel`),
  UNIQUE INDEX `login` (`login` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;


-- -----------------------------------------------------
-- Table `excel`.`Firma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`Firma` (
  `id_firma` INT(11) NOT NULL AUTO_INCREMENT,
  `aktivni` BIT(1) NOT NULL,
  PRIMARY KEY (`id_firma`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;


-- -----------------------------------------------------
-- Table `excel`.`Dokument`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`Dokument` (
  `id_dokument` INT(11) NOT NULL AUTO_INCREMENT,
  `id_firma` INT(11) NOT NULL,
  `nazev` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `popis` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `soubor` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `vlozeno` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `autor` INT(11) NULL DEFAULT NULL,
  `typ` ENUM('Smlouva','Navrh','Jine') NULL,
  PRIMARY KEY (`id_dokument`),
  INDEX `id_firma` (`id_firma` ASC),
  INDEX `autor` (`autor` ASC),
  CONSTRAINT `Dokument_ibfk_2`
    FOREIGN KEY (`autor`)
    REFERENCES `excel`.`Uzivatel` (`id_uzivatel`)
    ON DELETE SET NULL,
  CONSTRAINT `Dokument_ibfk_1`
    FOREIGN KEY (`id_firma`)
    REFERENCES `excel`.`Firma` (`id_firma`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;


-- -----------------------------------------------------
-- Table `excel`.`Rocnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`Rocnik` (
  `id_rocnik` INT(11) NOT NULL AUTO_INCREMENT,
  `rok` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  PRIMARY KEY (`id_rocnik`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;


-- -----------------------------------------------------
-- Table `excel`.`VerzeFirmy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`VerzeFirmy` (
  `id_verze` INT(11) NOT NULL AUTO_INCREMENT,
  `id_firma` INT(11) NOT NULL,
  `rocnik` INT(11) NOT NULL,
  `nazev` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `ic` DECIMAL(10,0) NOT NULL,
  `dic` VARCHAR(45) NULL,
  `sponzorstvi` ENUM('nedojednano','bronzove','stribrne','zlate') CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `benefity` SET('stanek','logo','plakat','ucast') NOT NULL,
  `pozadavky` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `komunikace` MEDIUMTEXT CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `soucasna` BIT(1) NOT NULL,
  `zmeneno` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `autor` INT(11) NULL,
  `penize_prislibeno` DECIMAL NOT NULL,
  `penize_prijato` DECIMAL NOT NULL,
  `url` TEXT NOT NULL,
  `prubeh` ENUM('kontaktovana','jednani','smlouva','dokonceno') COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id_verze`),
  INDEX `id_firma` (`id_firma` ASC),
  INDEX `rocnik` (`rocnik` ASC),
  INDEX `autor` (`autor` ASC),
  CONSTRAINT `VerzeFirmy_ibfk_1`
    FOREIGN KEY (`id_firma`)
    REFERENCES `excel`.`Firma` (`id_firma`)
    ON DELETE CASCADE,
  CONSTRAINT `VerzeFirmy_ibfk_4`
    FOREIGN KEY (`rocnik`)
    REFERENCES `excel`.`Rocnik` (`id_rocnik`)
    ON DELETE CASCADE,
  CONSTRAINT `VerzeFirmy_ibfk_5`
    FOREIGN KEY (`autor`)
    REFERENCES `excel`.`Uzivatel` (`id_uzivatel`)
    ON DELETE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;


-- -----------------------------------------------------
-- Table `excel`.`VerzeUzivatele`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`VerzeUzivatele` (
  `id_verze_uzivatele` INT(11) NOT NULL AUTO_INCREMENT,
  `id_uzivatel` INT(11) NOT NULL,
  `titul` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `jmeno` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `prijmeni` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `telefon` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_czech_ci' NOT NULL,
  `soucasna` BIT(1) NOT NULL,
  `zmeneno` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `autor` INT(11) NULL,
  `role` ENUM('firma','asistent','admin') NOT NULL,
  PRIMARY KEY (`id_verze_uzivatele`),
  INDEX `id_uzivatel` (`id_uzivatel` ASC),
  INDEX `autor` (`autor` ASC),
  CONSTRAINT `VerzeUzivatele_ibfk_1`
    FOREIGN KEY (`id_uzivatel`)
    REFERENCES `excel`.`Uzivatel` (`id_uzivatel`)
    ON DELETE CASCADE,
  CONSTRAINT `VerzeUzivatele_ibfk_2`
    FOREIGN KEY (`autor`)
    REFERENCES `excel`.`Uzivatel` (`id_uzivatel`)
    ON DELETE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;


-- -----------------------------------------------------
-- Table `excel`.`Adresy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`Adresy` (
  `id_adresy` INT(11) NOT NULL AUTO_INCREMENT,
  `popis` VARCHAR(45) NOT NULL,
  `adresa` TEXT NOT NULL,
  PRIMARY KEY (`id_adresy`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `excel`.`firmy_adresy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`Firmy_Adresy` (
  `VerzeFirmy_id_verze` INT(11) NOT NULL,
  `adresy_id_adresy` INT(11) NOT NULL,
  INDEX `fk_firmy_adresy_VerzeFirmy1_idx` (`VerzeFirmy_id_verze` ASC),
  INDEX `fk_firmy_adresy_adresy1_idx` (`adresy_id_adresy` ASC),
  CONSTRAINT `fk_firmy_adresy_VerzeFirmy1`
    FOREIGN KEY (`VerzeFirmy_id_verze`)
    REFERENCES `excel`.`VerzeFirmy` (`id_verze`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_firmy_adresy_adresy1`
    FOREIGN KEY (`adresy_id_adresy`)
    REFERENCES `excel`.`Adresy` (`id_adresy`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `excel`.`firmy_uzivatele`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `excel`.`Firmy_Uzivatele` (
  `id_verze` int(11) NOT NULL,
  `id_verze_uzivatele` int(11) NOT NULL,
  `popis` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  INDEX `fk_firmy_uzivatele_VerzeFirmy1_idx` (`id_verze` ASC),
  INDEX `fk_firmy_uzivatele_VerzeUzivatele1_idx` (`id_verze_uzivatele` ASC),
  CONSTRAINT `fk_firmy_uzivatele_VerzeFirmy1`
    FOREIGN KEY (`id_verze`)
    REFERENCES `excel`.`VerzeFirmy` (`id_verze`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_firmy_uzivatele_VerzeUzivatele1`
    FOREIGN KEY (`id_verze_uzivatele`)
    REFERENCES `excel`.`VerzeUzivatele` (`id_verze_uzivatele`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- View `excel`.`DokumentFirmaView`
-- -----------------------------------------------------
CREATE VIEW `excel`.`DokumentFirmaView` AS
SELECT `Dokument`.`id_dokument` id, `Dokument`.*, `VerzeFirmy`.`id_verze` `firma_id_verze`, `VerzeFirmy`.`nazev` `firma_nazev`,
`VerzeFirmy`.`rocnik` `firma_rocnik`, `VerzeFirmy`.`ic` `firma_ic`, `VerzeFirmy`.`dic` `firma_dic` 
FROM `Dokument` 
LEFT JOIN `Firma` ON `Dokument`.`id_firma` = `Firma`.`id_firma` 
LEFT JOIN `VerzeFirmy` ON `Firma`.`id_firma` = `VerzeFirmy`.`id_firma` 
WHERE (`VerzeFirmy`.`soucasna` = 1);


-- -----------------------------------------------------
-- Testing data
-- -----------------------------------------------------
INSERT INTO `Uzivatel` (`id_uzivatel`, `login`, `heslo`) VALUES
(1, 'admin', '$2y$10$onjP9kVDXWf6jQapifnFv.KWnL6cSnLZBrF1vGsoTs.asV9uVwntG');
INSERT INTO `VerzeUzivatele` (`id_verze_uzivatele`, `id_uzivatel`, `titul`, `jmeno`, `prijmeni`, `telefon`, `email`, `soucasna`, `zmeneno`, `autor`, `role`) VALUES
(1,	1,	'',	'Admin',	'Administrátorovič',	'',	'muj@mail.cz',	1,	'2015-04-27 12:21:39',	NULL,	'admin');
INSERT INTO `Rocnik` (`id_rocnik`, `rok`) VALUES
(1,	'2013');
INSERT INTO `Firma` (`id_firma`, `aktivni`) VALUES
(1,	1);
INSERT INTO `VerzeFirmy` (`id_verze`, `id_firma`, `rocnik`, `nazev`, `ic`, `dic`, `sponzorstvi`, `benefity`, `pozadavky`, `komunikace`, `soucasna`, `zmeneno`, `autor`, `penize_prislibeno`, `penize_prijato`, `prubeh`) VALUES
(1,	1,	1,	'test',	0,	NULL,	'nedojednano',	'',	'',	'',	1,	NULL,	NULL,	0,	0,	'smlouva');
INSERT INTO `Dokument` (`id_dokument`, `id_firma`, `nazev`, `popis`, `soubor`, `vlozeno`, `autor`, `typ`) VALUES
(1,	1,	'tst',	'ssss',	'xxx.yy',	'2015-04-26 17:27:42',	1,	'Smlouva');

