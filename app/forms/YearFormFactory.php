<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form;


class YearFormFactory extends Nette\Object
{
	/**
	 * @return Form
	 */
	public function create() {
		$form = new Form;
		$form->addText('rok', 'Rok:')
        	->addRule(Form::FILLED, 'Zadejte rok!')
			->addRule(Form::INTEGER, 'Rok musí být číslo!')
			->addRule(Form::RANGE, 'Zadejte platný rok!', array(1900, 3000));
		return $form;
	}

}
