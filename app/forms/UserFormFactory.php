<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form;


class UserFormFactory extends Nette\Object
{
	/**
	 * @return Form
	 */
	public function create() {
		$form = new Form;
        $form->addText('firstname', 'Jméno')
             ->addRule(Form::FILLED, 'Vyplňte jméno')
             ->addRule(Form::LENGTH, 'Jméno musí být %d až %d znaků dlouhé', array(2,30));
        $form->addText('lastname', "Příjmení")
             ->addRule(Form::FILLED, 'Vyplňte příjmení')
             ->addRule(Form::LENGTH, 'Příjmení musí být %d až %d znaků dlouhé', array(2,40));
        $form->addText('degree', "Titul");
        $form->addText('username', 'Login')
             ->addRule(Form::FILLED, 'Vyplňte přihlašovací jméno')
             ->addRule(Form::LENGTH, 'Jméno musí mít %d až %d znaků dlouhé', array(4,20));
        $form->addPassword('password', 'Heslo');
        $form->addText('phone', 'Telefon');
        $form->addText('email', 'Email')
             ->addCondition(Form::FILLED)
             ->addRule(Form::EMAIL, "Musí být platný email");
        $role = array(
            'admin' => 'Admin',
            'asistent' => 'Asistent',
            'firma' => 'Zástupce firmy'
        );
        $form->addRadioList('role', 'Role', $role)
             ->addRule(Form::FILLED, 'Vyplňte roli');
		return $form;
	}

}
