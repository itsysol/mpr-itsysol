<?php

namespace App\Controls;

use Nette,
	App\Model,
	App\Model\Documents,
	Grido\Translations\FileTranslator,
	Grido\Components\Filters\Filter,
	Nette\Database\Table\Selection,
	Grido\Components\Columns\Date,
	Grido\Grid;

class DocumentsGrid extends Grid
{
	private $documents;
	
	public function __construct($parent, $name, $documents, $id_firma = NULL)
	{
		parent::__construct($parent, $name);
		$this->documents = $documents;
		
		$this->setModel($this->documents->getTableWithCompany($id_firma));
		$this->setTranslator(new FileTranslator('cs'));
		$this->setFilterRenderType(Filter::RENDER_INNER);
		$this->setDefaultSort(array('vlozeno' => 'DESC'));
		
		$this->addFilterText('nazev', 'Název');
		$this->addFilterSelect('typ', 'Typ', $this->documents->typ);
		$this->addFilterText('firma_nazev', 'Název');
		$this->addFilterDateRange('vlozeno', 'Vloženo');
		
		$this->addColumnText('nazev', 'Název')->setSortable();
		$this->addColumnText('typ', 'Typ')->setSortable()
		     ->setCustomRender(function($row){ return $this->documents->typ[$row->typ]; });;
		$this->addColumnDate('vlozeno', 'Vloženo')->setDateFormat(Date::FORMAT_DATETIME)->setSortable();
		
		if($id_firma == NULL){
			$this->addColumnText('firma_nazev', 'Firma')->setSortable();
		}
		
		$this->addActionHref('download', 'Stáhnout', 'Documents:download');
		$this->addActionHref('modify', 'Upravit', 'Documents:modify');
		$this->addActionHref('remove', 'Odstranit', 'Documents:remove');
		return $this;
	}
	
}
