<?php

namespace App\Model;

use Nette;

class Years extends Nette\Object
{
	protected $context;
	
	public function __construct(Nette\Database\Context $context)
	{
		$this->context = $context;
	}
	
	public function getTable()
	{
		return $this->context->table("Rocnik");
	}

    /**
     * Serazena posloupnost rocniku podle rocniku
     */
	public function getYears() {
    	return $this->getTable()->group('rok')->order('rok DESC');
    }

	public function add($values) {
    	$newUser = $this->getTable()->insert(array(
        				'rok' => $values->rok
        			));
    }	

    /**
     * Databazovy identifikator aktivniho rocniku
     */
    public function getLastId() {
        $id = $this->getTable()->select('id_rocnik')->order("id_rocnik DESC")->limit(1)->fetch();
        if($id) 
        {
            return $id->id_rocnik;
        }
        
        return 0;
    }
}
