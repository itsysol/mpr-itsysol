<?php

namespace App\Model;

use Nette,
    Nette\Security as NS,
	Nette\Utils\Strings;

/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements NS\IAuthenticator
{
    
	const
		MAIN_TABLE_NAME = 'Uzivatel',
        DATA_TABLE_NAME = 'VerzeUzivatele',
		COLUMN_ID = 'id_uzivatel',
		COLUMN_LOGIN = 'login',
		COLUMN_PASSWORD_HASH = 'heslo',
		COLUMN_ROLE = 'role',
        COLUMN_VALID = 'soucasna';
    
    
	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;
        $row = $this->database->table(self::MAIN_TABLE_NAME)->where(self::COLUMN_LOGIN, $username)->fetch();

        if (!$row || !NS\Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new NS\AuthenticationException('Chybné přihlášení.', self::INVALID_CREDENTIAL);
		}

        $version = $row->related(self::DATA_TABLE_NAME,self::COLUMN_ID)
                       ->where(self::COLUMN_VALID)->fetch();
        $data = $version->toArray();
        $data["login"] = $row->login;
        $roles = [$version->role];
        
        return new NS\Identity($row[self::COLUMN_ID], $roles, $data);
	}
    /**
	 * Adds new user. Project initialization method
	 * @param  string
	 * @param  string
	 * @return void
	 */
	public function add($username, $password, $role)
	{
		try {
			$inserted = $this->database->table(self::MAIN_TABLE_NAME)->insert(array(
                self::COLUMN_LOGIN => $username,
				self::COLUMN_PASSWORD_HASH => NS\Passwords::hash($password),
			));
            $version = $this->database->table(self::DATA_TABLE_NAME)->insert(array(
                self::COLUMN_ID => $inserted[self::COLUMN_ID],
                "jmeno" => $username,
                "prijmeni" => $username,
                "telefon" => "",
                "email" => "",
                "soucasna" => 1,
                "autor" => $inserted[self::COLUMN_ID],
                "role" => $role
            ));
            
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}
}
