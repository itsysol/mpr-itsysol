<?php

namespace App\Model;

use Nette;

/** Rozhraní pro firmy v databázi */
class Companies extends Nette\Object {

    protected $context;
    public $sponzorstvi = array(
            "" => "",
            "nedojednano" => "Nedojednáno",
            "bronzove" => "Bronzové",
            "stribrne" => "Stříbrné",
            "zlate" => "Zlaté"
    );

    public $benefityBase = array(
            "stanek" => "Stánek",
            "logo" => "Logo",
            "plakat" => "Plakát",
            "ucast" => "Účast"
    );

    public $benefity = array(
            "" => "",
            "stanek" => "Stánek",
            "logo" => "Logo",
            "plakat" => "Plakát",
            "ucast" => "Účast"
    );


    public $prubeh = array(
            "" => "Dosud nekontaktována",
            "dokonceno" => "Vyjednáno",
            "smlouva" => "Smlouva",
            "jednani" => "V jednání",
            "kontaktovana" => "Kontaktována"

    );

    public function __construct(Nette\Database\Context $context)
    {
            $this->context = $context;
    }
    
    /** Získej hlavní tabulku firem */
    public function getTable()
    {
            return $this->context->table("Firma");
    }

    /** Získej tabulku s verzemi firem */
    public function getVersionTable()
    {
            return $this->context->table("VerzeFirmy");
    }

    /** Získej tabulku s adresami firem */
    private function getAdressTable()
    {
            return $this->context->table("Adresy");
    }

    /** Získej propojovací tabulku adres a verzí firem */
    private function getCompanyAdressTable()
    {
            return $this->context->table("Firmy_Adresy");
    }

    /** Získej propojovací tabulku verzí uživatelů a verzí firem */
    private function getCompanyUsersTable()
    {
            return $this->context->table("Firmy_Uzivatele");
    }

    /** Přidej záznam do hlavní tabulky a vytvoř aktuální verzi */
    public function add($data, $adresses, $users, $id) {
        $this->context->beginTransaction();
        //vytvoreni firmy
        $entry_company = $this->getTable()->insert(array(
                'aktivni' => TRUE
        ));

        //priprava dat
        $data->id_firma = $entry_company->id_firma;

        //vytvoreni verze firmy
        $this->createEntry($data, $adresses, $users, $id);

        $this->context->commit();
    }
        
    /**
     * Ziskani soucasne verze firmy
     * @param int $id
     * @return array  Data
     */
    public function getCurrentVersionData($id)
    {   
        $data = $this->getVersionTable()->where('id_firma', $id)->where('soucasna', TRUE)->fetch();

        if($data) {
            $row = $data->toArray();
            $row['benefity'] = $data->benefity ? explode(",", $data->benefity) : array();
            return $row;
        }

        return $data;
    }

    /**
     * Ziskani firem pro dany rocnik
     * @param int $year_id
     * @return arraz  Data
     */
    public function getCompaniesForYear($year_id)
    {   
		$data = $this->context->query(
			'SELECT Uzivatel.login, VerzeFirmy.*
				FROM VerzeFirmy
				LEFT JOIN Uzivatel
				ON Uzivatel.id_uzivatel = VerzeFirmy.autor
				WHERE VerzeFirmy.rocnik = ?
				GROUP BY VerzeFirmy.id_firma', $year_id);		

        return $data;
    }

    /**
     * Ziskani zazanamu firem ve vsech rocnicich
     */
	public function getCompaniesForYears()
    {   
		$data = $this->context->query(
			'SELECT Uzivatel.login, VerzeFirmy.*, Rocnik.rok
				FROM VerzeFirmy
				LEFT JOIN Uzivatel
				ON Uzivatel.id_uzivatel = VerzeFirmy.autor
				LEFT JOIN Rocnik
				ON Rocnik.id_rocnik = VerzeFirmy.rocnik
				GROUP BY VerzeFirmy.id_firma');		

        return $data;
    }

	/**
     * Ziskani seznamu historii firmy
     * @param type $year_id
     * @return type
     */
    public function getCompanyHistories($id_firma)
    {   
		$data = $this->context->query(
			'SELECT Uzivatel.login, VerzeFirmy.*
				FROM VerzeFirmy
				LEFT JOIN Uzivatel
				ON Uzivatel.id_uzivatel = VerzeFirmy.autor
				WHERE VerzeFirmy.id_firma = ?
				ORDER BY VerzeFirmy.zmeneno ASC', $id_firma);		

        return $data;
    }

	/**
     * Ziskani historie firmy
     * @param type $year_id
     * @return type
     */
    public function getCompanyHistory($id_verze)
    {   
		$data = $this->context->query(
			'SELECT *
				FROM VerzeFirmy
				WHERE id_verze = ?', $id_verze);		

        return $data;
    }

    public function getCurrentVersionDataRaw($id)
    {   
        return $this->getVersionTable()->where('id_firma', $id)->where('soucasna', TRUE)->fetch();
    }
        
    /**
     * Ziskani adres verze firmy
     * @param type $id
     * @return type
     */
    public function getCurrentVersionAdresses($id)
    {   
        return $data = $this->getCompanyAdressTable()->select("adresy_id_adresy.adresa AS adresa,adresy_id_adresy.popis AS popis")->where('VerzeFirmy_id_verze', $id)->fetchAll();
    }
    
    /**
     * Ziskani soucasne verze firmy
     * @param type $id
     * @return type
     */
    public function getCurrentVersionUsers($id)
    {   
		return $this->context->query(
			'SELECT VerzeUzivatele.*, Firmy_Uzivatele.popis, Uzivatel.login
				FROM Firmy_Uzivatele
				LEFT JOIN VerzeUzivatele
				ON VerzeUzivatele.id_verze_uzivatele = Firmy_Uzivatele.id_verze_uzivatele
				LEFT JOIN Uzivatel
				ON Uzivatel.id_uzivatel = Firmy_Uzivatele.id_verze_uzivatele
				WHERE Firmy_Uzivatele.id_verze = ?', $id);
    }
        
    /**
     * Aktualizace(pridani nove verze) firmy
     * @param type $data
     */
    public function update($data, $adresses, $users, $id) {
        $this->context->beginTransaction();

        //update stare verze
        $this->getVersionTable()->where('id_verze', $data->id_verze)->update(array('soucasna' => FALSE));

        //priprava data
        $data->id_verze = NULL;

        //vytvoreni nove
        $this->createEntry($data, $adresses, $users, $id);

        $this->context->commit();
    }

    /**
     * @return ActiveRow New version row
     */
    public function createNewVersion($id, $author) {
        $data = $this->getCurrentVersionData($id);
        $data = json_decode(json_encode($data), FALSE);
        $adresses = $this->getCurrentVersionAdresses($data->id_verze);
        $users = $this->getCurrentVersionUsers($data->id_verze);
        $this->getVersionTable()->where('id_verze', $data->id_verze)->update(array('soucasna' => FALSE));
        $data->id_verze = NULL;
        $created = $this->createEntry($data, array(), array(), $author);
        foreach ($adresses as $adress) {
            $this->getCompanyAdressTable()
                 ->inser(array(
                     "VerzeFirmy_id_verze" => $created->id_verze,
                     "adresy_id_adresy" => $adress->adresy_id_adresy
                 ));
        }
        foreach ($users as $user) {
            $this->getCompanyUsersTable()
                 ->insert(array(
                     "id_verze" => $created->id_verze,
                     "id_verze_uzivatele" => $user->id_verze_uzivatele,
                     "popis" => $user->popis
                 ));
        }
        return $created;
    }

    /**
     * Vytvoreni zaznamu verze firmy vcetne vsech ostatnich zaznamu. 
     * Pouzitelne pro novou i verzovanou firmu. 
     * @param type $data
     * @param type $adresses
     * @param type $users
     * @param type $id
     */
    private function createEntry($data, $adresses, $users, $id) {
        //vseobecne nastaveni (unikatni nastaveni pred volanim funkce)
        $data->benefity = implode(",", $data->benefity);
        $data->soucasna = TRUE;
        $data->autor = $id;

        //zaznam verze firmy
        $entry_version = $this->getVersionTable()->insert(array_intersect_key(
                        (array) $data, array_flip(array(
            'id_firma',
            'rocnik',
            'nazev',
            'ic',
            'dic',
            'sponzorstvi',
            'benefity',
            'pozadavky',
            'komunikace',
            'soucasna',
            'penize_prislibeno',
            'penize_prijato',
            'prubeh',
            'url',
            'autor'))));

        //zaznamy adress
        foreach ($adresses as $value) {
                $entry_adress = $this->getAdressTable()->insert($value);

                $this->getCompanyAdressTable()->insert(array(
                    'VerzeFirmy_id_verze' => $entry_version->id_verze,
                    'adresy_id_adresy' => $entry_adress->id_adresy
                ));
        }


        //zaznamy uzivatelu
        foreach ($users as $value) {
                $this->getCompanyUsersTable()->insert(array(
                    'id_verze' => $entry_version->id_verze,
                    'id_verze_uzivatele' => $value['uzivatel'],
                    'popis' => $value['popis']
                ));
        }
        return $entry_version;
    }

    /**
     * Odstraneni(deaktivace) firmy
     * 
     * @param type $id
     */
    public function remove($id) {
        $this->context->beginTransaction();

        //update: deaktivace firmy
        $this->getTable()->where('id_firma', $id)->update(array('aktivni' => FALSE));

        $this->context->commit();
    }

}
