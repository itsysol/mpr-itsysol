<?php

namespace App\Model;

use Nette,
    Nette\Database\Context;

class Documents extends Nette\Object
{
	protected $context;
	protected $files_path = 'files';
	
	public $typ = array(
		"" => "",
		"Smlouva" => "Smlouva",
		"Navrh" => "Návrh",
		"Jine" => "Jiné"
	);
		
	public function __construct(Context $context)
	{
		$this->context = $context;
	}
	
	public function getTable()
	{
		return $this->context->table("Dokument");
	}

    /* Databazovy zaznam o dokumentu */
	public function get($id)
	{
		return $this->getTable()->where("id_dokument",$id)->fetch();
	}

    /* Pridej dokument jako specifikovany uzivatel */
	public function add($data,$userId) {
		 $this->context->beginTransaction();
		 $data->autor = $userId;
		 
		 $row = $this->getTable()->insert(array($data));

		 if ($data['soubor']->isOk()) {
			$filename = $row->id_dokument."-".$data['soubor']->getSanitizedName();

			$data['soubor']->move("{$this->files_path}/$filename");

			$data->soubor = $filename;
		 }
		 $row->update($data);
		 
		 $this->context->commit();
	}

    /* Dokumenty prislusejici prave dane firme */
	public function getTableWithCompany($company = NULL)
	{
		$table = $this->context->table("DokumentFirmaView");
		if($company) $table->where("id_firma",$company);
		return $table;
		/*
		return $this->context->table("Dokument")->select("Dokument.*")
		            ->select("Firma:VerzeFirmy.id_verze firma_id_verze")
		            ->select("Firma:VerzeFirmy.nazev firma_nazev")
		            ->select("Firma:VerzeFirmy.rocnik firma_rocnik")
		            ->select("Firma:VerzeFirmy.ic firma_ic")
		            ->select("Firma:VerzeFirmy.dic firma_dic")
		            ->where("Firma:VerzeFirmy.soucasna",1);
		*/
	}

    /** Zanes zmenu do databaze */
	public function change($id, $author, $values) {
		$this->getTable()->where("id_dokument",$id)->update($values);
	}

    /** Odstran zaznam z databaze a soubor */
	public function remove($id){
		$this->context->beginTransaction();
		$values = $this->get($id);
		unlink($this->getUrlByName($values->soubor));
		$this->getTable()->where("id_dokument",$id)->delete();
		$this->context->commit();
	}

    /** cesta pro ulozeni souboru v systemu */
    public function getUrlByName($name){
        return "{$this->files_path}/{$name}";
    }

}
