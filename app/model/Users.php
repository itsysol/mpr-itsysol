<?php

namespace App\Model;

use Nette,
    Tracy\Debugger,
    Nette\Security as NS;

class Users extends Nette\Object
{
	protected $context;
    protected $companies;
	
	public function __construct(Nette\Database\Context $context, Companies $companies)
	{
		$this->context = $context;
        $this->companies = $companies;
	}
	
	public function getTable()
	{
		return $this->context->table("Uzivatel");
	}
	
	public function getVersionTable()
	{
		return $this->context->table("VerzeUzivatele");
	}

    /** Ziskej hlavni zaznam uzivatele */
    public function get($id) {
        return $this->getTable()->get($id);
    }

    /**
     * Ziskej data soucasne verze
     */
    public function getCurrentVersion($id) {
        return $this->getVersionTable()->where('id_uzivatel', $id)->where('soucasna')->fetch();
    }

    /**
     * Pole firem u kterych je uveden v soucasnych verzich
     */
    public function getRelatedCompanies($id)
    {
        $companies = [];
        $users = $this->getVersionTable()->where("id_uzivatel", $id)->where("soucasna");
        foreach ($users as $userVersion) {
            $companyVersions = [];
            foreach ($userVersion->related("Firmy_Uzivatele", "id_verze_uzivatele")->select("id_verze") as $version) {
                $companyVersions[] = $version->id_verze;
            }
            $filteredCompanies = $this->context->table("VerzeFirmy")->where("id_verze", $companyVersions)->where("soucasna");
            foreach ($filteredCompanies as $company) {
                $companies[] = $company->id_firma;
            }
        }
        return $companies;
    }

    public function calculateHash($password) {
		return NS\Passwords::hash($password);
    }

    public function isUsernameAvailable($username) {
        if (!is_string($username)) {
            $username = $usename->value;
        }
        return (bool) $this->getTable()->where('login', $username)->count() == 0;
    }

    public function createPassword() {
        return Nette\Utils\Strings::random();
    }

    /**
     * Pridej uzivatele jako autor zmen
     */
    public function add($values, $author) {
        try {
            $this->context->beginTransaction();
             if (!$this->isUsernameAvailable($values->username)) {
                 throw new \Exception('Uživatelské jméno je obsazeno');
             } else {
                 if (empty($values->password)) {
                     $newpass = $this->createPassword();
                 } else {
                     $newpass = $values->password;
                 }
                 
                 $newUser = $this->getTable()->insert(array(
                     'login' => $values->username,
                     'heslo' => $this->calculateHash($newpass)
                 ));
                 $this->getVersionTable()->insert(array(
                     'id_uzivatel' => $newUser->id_uzivatel,
                     'titul' => $values->degree,
                     'jmeno' => $values->firstname,
                     'prijmeni' => $values->lastname,
                     'telefon' => $values->phone,
                     'email' => $values->email,
                     'soucasna' => 1,
                     'autor' => $author,
                     'role' => $values->role
                 ));
            }
            $this->context->commit();
            if (empty($values->password)) {
                return array($newUser, $newpass);
            } else {
                return array($newUser,"");
            }
        } catch (\Exception $e) {
            $this->context->rollback();
            throw $e;
        }
    }

    /**
     * Vytvor novou verzi uzivatele jako autor zmen
     */
    public function change($id, $author, $values) {
        try {
            $this->context->beginTransaction();
            $edited = $this->get($id);
            $editedVersion = $this->getCurrentVersion($id);
            $related = $this->getRelatedCompanies($id);
            if ($edited->login != $values->username) { // username changed
                if (!$this->isUsernameAvailable($values->username)) {
                    throw new \Exception('Uživatelské jméno je obsazeno');
                } else {
                    if (!$edited->update(array('login' => $values->username))) {
                        throw new \Exception('Uživatelské jméno je obsazeno');
                    }
                }
            }
            if (!empty($values->password)) {
                $edited->update(array(
                    'heslo' => $this->calculateHash($values->password)
                ));
            }
            $editedVersion->update(array(
                'soucasna' => 0
            ));
            $newVersion = $this->getVersionTable()->insert(array(
                'id_uzivatel' => $edited->id_uzivatel,
                'titul' => $values->degree,
                'jmeno' => $values->firstname,
                'prijmeni' => $values->lastname,
                'telefon' => $values->phone,
                'email' => $values->email,
                'soucasna' => 1,
                'autor' => $author,
                'role' => $values->role
            ));
            if ($related != NULL) {
                foreach ($related as $compId) {
                    $newCompany = $this->companies->createNewVersion($compId, $author);
                    $this->context->table("Firmy_Uzivatele")
                                  ->where("id_verze", $newCompany->id_verze)
                                  ->where("id_verze_uzivatele", $editedVersion->id_verze_uzivatele)
                                  ->update(array("id_verze_uzivatele" => $newVersion->id_verze_uzivatele));
                }
            }
            $this->context->commit();
        } catch (\Exception $e) {
            $this->context->rollback();
            throw $e;
        }
    }

    /**
     * Skryj (smaz) uzivatele
     */
    public function hide($id) 
    {
        try {
            $this->context->beginTransaction();
            $this->getTable()->where('id_uzivatel', $id)->update(array('heslo' => ''));
            $this->getVersionTable()->where('id_uzivatel', $id)->where('soucasna')->update(array('soucasna' => 0));
            $this->context->commit();
        } catch (\Exception $e) {
           $this->context->rollback();
           throw $e;
        }
    }
    
    public function getUsersSelectionList() 
    {
        return $this->getVersionTable()->select('id_verze_uzivatele, CONCAT_WS(" ",prijmeni,jmeno) AS hodnota')->where('soucasna', TRUE)->fetchPairs('id_verze_uzivatele', 'hodnota');
    }
    

}
