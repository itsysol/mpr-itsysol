<?php

namespace App\Model;

use Nette;

abstract class BaseRepository extends Nette\Object {
    
    /** @var Nette\Database\Context */
    protected $context;
    
    public function __construct(Nette\Database\Connection $context)
	{
		$this->context = $context;
	}
    
    
    
}
