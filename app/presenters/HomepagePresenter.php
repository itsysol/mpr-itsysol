<?php

namespace App\Presenters;

use Nette,
	App\Model,
    App\Model\Users,
	App\Forms\SignFormFactory;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
	/** @var SignFormFactory @inject */
	public $factory;
    public $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Presmeruj na defaultni stranku n zaklade identity uzivatele
     */
	public function renderDefault()
	{
        $user = $this->getUser();
        if ($user->isLoggedIn()) {
            if ($user->isInRole('firma')) {
                $companies = $this->users->getRelatedCompanies($user->id);
                if (empty($companies)) {
                    $this->flashMessage("Nejste přiřazen žádné firmě v aktuálním ročníku", "warning");
                    $this->redirect("Sign:out"); // no related company found, just sign out
                } else {
                    $this->redirect("Companies:show", array("id" => $companies[0]));
                }
            } else {
                $this->redirect("Companies:");
            }
        }
	}

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = $this->factory->create();
		$form->onSuccess[] = function ($form) {
            $this->flashMessage('Přihlášení bylo úspěšné.', 'success');
			$form->getPresenter()->redirect('Homepage:');
        };
		return $form;
	}
}
