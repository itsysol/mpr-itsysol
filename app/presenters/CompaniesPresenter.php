<?php

namespace App\Presenters;

use Nette,
	App\Model,
	App\Model\Companies,
        App\Model\Users,
        App\Model\Years,
        Nette\Application\UI\Form,
	Grido\Translations\FileTranslator,
	Nette\Database\Table\Selection,
	App\Model\Documents,
	App\Controls\DocumentsGrid;
        
class CompaniesPresenter extends PrivatePresenter
{
	public $id_firma;
	private $companies;
	private $documents;
        private $users;
        private $years;
        
	public function __construct(Companies $companies, Documents $documents, Users $users, Years $years)
	{
		$this->companies = $companies;
		$this->documents = $documents;
                $this->users = $users;
                $this->years = $years;
	}

    /* Uzivatelska akce obrazovky zmeny */
	public function actionEdit($id)
	{
		$this->id_firma = $id;
	}

    /* Uzivatelska akce obrazovka detailu */
    public function actionShow($id) {
        if ($this->getUser()->isInRole('firma')) {
            $this->setView('restrictedShow'); 
        }   
        $this->id_firma = $id;     
    }

    /* Vytvor obrazovku pridani */
    public function renderAdd() {
        $id_rocnik = $this->years->getLastId();
        if($id_rocnik == 0) {
            $this->flashMessage("Nelze vytvorit -> Neexistujici rocnik!");
            $this->redirect('Companies:default');
        } else {
            $form = $this->getComponent("newForm");
            $form->getComponent("rocnik")->setDefaultValue($id_rocnik);
        }
    }

    /* Obrazovka detailu pro firmy */
    public function renderRestrictedShow($id) {
        $form = $this->getComponent("showForm");
        //data z verze
        $version = $this->companies->getCurrentVersionData($id);
        $form->setDefaults($version);
        //data z adres
        $this->extendFormAdresses($form,$this->companies->getCurrentVersionAdresses($version["id_verze"]));
        //data z uzivatelu
        $this->extendFormUsers($form, $this->companies->getCurrentVersionUsers($version["id_verze"]));
        foreach ($form->getControls() as $control) {
            $control->controlPrototype->readonly = 'readonly';
        }
    }
    
    /* Obrazovka detailu pro fit */
    public function renderShow($id) {
        $form = $this->getComponent("showForm");
        //data z verze
        $version = $this->companies->getCurrentVersionData($id);
        $form->setDefaults($version);
        //data z adres
        $this->extendFormAdresses($form,$this->companies->getCurrentVersionAdresses($version["id_verze"]));
        //data z uzivatelu
        $this->extendFormUsers($form, $this->companies->getCurrentVersionUsers($version["id_verze"]));
        foreach ($form->getControls() as $control) {
            $control->controlPrototype->readonly = 'readonly';
        }
    }

    /* Obrazovka historie - jedna firma */
    public function renderHistory($id) {
		//nacteni historie
            $hist = $this->companies->getCompanyHistories($id);
		$out = false;
		//nacteni jednotlivych verzi
		foreach($hist as $y)
		{
			$outLine['id_verze'] = $y['id_verze'];
			$outLine['zmeneno'] = $y['zmeneno'];
			$outLine['login'] = $y['login'];
			$outLine['nazev'] = $y['nazev'];
			$outLine['ic'] = $y['ic'];
			$outLine['dic'] = $y['dic'];
			$outLine['sponzorstvi'] = $y['sponzorstvi'];
			$outLine['benefity'] = $y['benefity'];
			if ($outLine['benefity'])
				$outLine['benefity'] = explode(",", $y['benefity']);
			else
				$outLine['benefity'] = [];
			$outLine['pozadavky'] = $y['pozadavky'];
			$outLine['prubeh'] = $y['prubeh'];
			$outLine['komunikace'] = $y['komunikace'];
			$outLine['penize_prislibeno'] = $y['penize_prislibeno'];
			$outLine['penize_prijato'] = $y['penize_prijato'];
			$outLine['addresses'] = $this->companies->getCurrentVersionAdresses($outLine['id_verze']);
			$outLine['users'] = $this->companies->getCurrentVersionUsers($outLine['id_verze']);
			$out[] = $outLine;
		}
		$this->template->history = $out;
    }

    /* Komponenta tabulka historie */
	protected function createComponentHistoryTable($name)
    {
        $grid = new \Grido\Grid($this, $name);
		$grid->setModel($this->companies->getVersionTable());
		$grid->setTranslator(new FileTranslator('cs'));
		$grid->setDefaultSort(array('nazev' => 'ASC'));
		
		$grid->addColumnText('zmeneno', 'Změněno');
		$grid->addColumnText('autor', 'Autor')->setSortable();
        
		return $grid;
    }

    /* obrazovka seznamu firem a statistik */
    public function renderDefault()
    {    
        // build statistics
        $sponsorship = [];
        foreach ($this->companies->sponzorstvi as $key => $name) {
            if ($key == "") continue;
            $sponsorship[$key] = array("name" => $name, "value" => 0);
        }
        $benefits = [];
        foreach ($this->companies->benefity as $key => $name) {
            if ($key == "") continue;
            $benefits[$key] = array("name" => $name, "value" => 0);
        }
        $money = array(
            "penize_prislibeno" => array("name" => "Přislíbeno", "value" => 0),
            "penize_prijato" => array("name" => "Přijato", "value" => 0)
        );
        $negotiations = [];
        foreach ($this->companies->prubeh as $key => $name) {
            $negotiations[$key] = array("name" => $name, "value" => 0);
        }

        $all = $this->companies->getVersionTable()->where('id_firma.aktivni')->where('soucasna')->where('rocnik', $this->years->getLastId());
        foreach ($all as $row) {
            $sponsorship[$row->sponzorstvi]["value"]++;
            $money["penize_prislibeno"]["value"] += $row->penize_prislibeno;
            $money["penize_prijato"]["value"] += $row->penize_prijato;
            $negotiations[$row->prubeh]["value"]++;
            if (!empty($row->benefity)) {
                foreach (explode(",", $row->benefity) as $benefit) {
                    $benefits[$benefit]["value"]++;
                }
            }
        }
        
        $this->template->sponsorship = $sponsorship;
        $this->template->benefits = $benefits;
        $this->template->money = $money;
        $this->template->negotiationState = $negotiations;
    }
        
    public function renderEdit($id) 
    {
        $form = $this->getComponent("updateForm");

        //data z verze
        $version = $this->companies->getCurrentVersionData($id);
        
        //zmena data v pripade neaktualniho rocniku
        $version['rocnik'] = $this->years->getLastId();
        
        $form->setDefaults($version);
        
        //data z adres
        $this->extendFormAdresses($form,$this->companies->getCurrentVersionAdresses($version["id_verze"]));
        
        //data z uzivatelu
        $this->extendFormUsers($form, $this->companies->getCurrentVersionUsers($version["id_verze"]));
        
    }

    public function actionRemove($id)
    {
            $this->companies->remove($id);
            $this->flashMessage("Firma odstraněna(deaktivovana).");
            $this->redirect('Companies:default');
    }
    
    public function renderChangeYear($id) 
    {
        $form = $this->getComponent("updateForm");

        //data z verze
        $version = $this->companies->getCurrentVersionData($id);
        $form->setDefaults($version);
        $form->getComponent("rocnik")->setDefaultValue($this->years->getLastId());
        //dump($form->getComponent("rocnik"));
        
        //data z adres
        $this->extendFormAdresses($form,$this->companies->getCurrentVersionAdresses($version["id_verze"]));
        
        //data z uzivatelu
        $this->extendFormUsers($form, $this->companies->getCurrentVersionUsers($version["id_verze"]));
    }

    protected function createComponentCompaniesGrid($name)
	{
		$grid = new \Grido\Grid($this, $name);
		$grid->setModel($this->companies->getVersionTable()->where('id_firma.aktivni = ? AND soucasna = ?', TRUE, TRUE));
		$grid->setTranslator(new FileTranslator('cs'));
		$grid->setDefaultSort(array('nazev' => 'ASC'));
		
		$grid->addFilterSelect('prubeh', 'Stav', $this->companies->prubeh);
		$grid->addFilterText('nazev', 'Název');
		$grid->addFilterSelect('sponzorstvi', 'Sponzorství', $this->companies->sponzorstvi);
		
		$grid->addFilterSelect('benefity', 'Benefity', $this->companies->benefity)
		     ->setWhere(function($value, $selection){
				return $selection->where('FIND_IN_SET(?,benefity)', $value);
		});
		
		$grid->addColumnText('prubeh', 'Stav')->setSortable()
		     ->setCustomRender(function($row){ return "<span class='tableico tableico-{$row->prubeh}'></span>"; });
		$grid->addColumnText('nazev', 'Název')->setSortable();
		$grid->addColumnText('sponzorstvi', 'Sponzorství')->setSortable()
		     ->setCustomRender(function($row){ return $row->sponzorstvi=="nedojednano" ? "" : "<span class='tableico tableico-{$row->sponzorstvi}'></span>"; });
		$grid->addColumnText('benefity', 'Benefity')
		     ->setCustomRender(function($row){
		         $cell = "";
		         if($row->benefity) foreach(explode(",",$row->benefity) as $benefit) $cell .= "<span class='tableico tableico-$benefit'></span>";
		         return $cell;
		       });
                    
                       
                $last_id_rocnik = $this->years->getLastId();
		$grid->addActionHref('show', 'Detail');
                $grid->addActionHref('changeYear', 'Přidat', 'Companies:edit')->setDisable(function($row) use ($last_id_rocnik) {return $last_id_rocnik == $row['rocnik'];});
		$grid->addActionHref('edit', 'Editace')->setDisable(function($row) use ($last_id_rocnik) {return $last_id_rocnik !== $row['rocnik'];});
		$grid->addActionHref('history', 'Historie')->setDisable(function($row) use ($last_id_rocnik) {return $last_id_rocnik !== $row['rocnik'];});;
		$grid->addActionHref('remove', 'Odstranit')->setDisable(function($row) use ($last_id_rocnik) {return $last_id_rocnik !== $row['rocnik'];});; 

                //mozno pridat "confirm dialog" pomoc setConfirm 
                
		return $grid;
	}
        
    /**
     * Vytvoreni zakladniho formulare
     * @return Form
     */
    private function createForm() 
    {
        $form = new Form;
        $form->getElementPrototype()->class('companyForm');

        $form->addHidden('id_firma');
        $form->addHidden('soucasna');
        $form->addHidden('autor');
        $form->addHidden('rocnik');

        $form->addText('nazev', 'Název:')
                ->setRequired('Zadejte název firmy.');
        $form->addText('ic', 'IČ:')
                ->addRule(Form::INTEGER, "IČ by mělo být číslo.")
                ->addRule(Form::LENGTH, "IČ by mělo být 8 místné číslo.", 8);

        $form->addText('dic', 'DIČ:');
	    $form->addText('url', 'URL:');

        $adressesBox = $form->addContainer('adressesBox');
        $adressBox = $adressesBox->addContainer('ID');
        $adressBox->addTextArea("adresa", "Adresa:");
        $adressBox->addText("popis", "Popis:");
        
        $usersBox = $form->addContainer('usersBox');
        $userBox = $usersBox->addContainer('ID');
        $userBox->addSelect("uzivatel", "Uživatel:", $this->users->getUsersSelectionList())->setPrompt("Zvolte...");
        $userBox->addText("popis", "Popis:");
            
        $form->addSelect("sponzorstvi", "Typ sponzorství:", $this->companies->sponzorstvi)
                ->setRequired('Zadejte typ sponzorství.')
                ->setDefaultValue('nedojednano');
        $form->addCheckboxList("benefity", "Benefity:", $this->companies->benefityBase)
             ->getSeparatorPrototype()->setName(NULL);

        $form->addText("penize_prislibeno", "Přislíbeno:")
                ->addCondition(Form::FILLED)
                ->addRule(Form::INTEGER, "Přislíbená částka musí být číslo.");

        $form->addText("penize_prijato", "Přijato:")
                ->addCondition(Form::FILLED)
                ->addRule(Form::INTEGER, "Přijatá částka musí být číslo.");

        $form->addTextArea("pozadavky", "Požadavky:");
        $form->addTextArea("komunikace", "Komunikace:");

        $form->addSelect("prubeh", "Průběh:", $this->companies->prubeh);

        return $form;
    }


    protected function createComponentNewForm()
    {
        $form = $this->createForm();

        $adressesBox = $form["adressesBox"];
        $adressBox = $adressesBox->addContainer('0');
        $adressBox->addTextArea("adresa", "Adresa:");
        $adressBox->addText("popis", "Popis:");
        
        $usersBox = $form["usersBox"];
        $userBox = $usersBox->addContainer('0');
        $userBox->addSelect("uzivatel", "Uživatel:", $this->users->getUsersSelectionList())->setPrompt("Zvolte...");
        $userBox->addText("popis", "Popis:");

        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->newFormSubmitted;
        return $form;
    }

    public function newFormSubmitted($form)
    {
        $id_grade = $this->years->getLastId();
        if($id_grade == 0) {
            
        } else {
            
        }
        
        $this->companies->add($form->getValues(), $this->parseFormAdresses($form), $this->parseFormUsers($form), $this->getUser()->getId());
        $this->flashMessage("Firma přidána!");
        $this->redirect('Companies:default');
    }

    protected function createComponentShowForm()
    {
        $form = $this->createForm();
        $form->addHidden('id_verze');
        
        return $form;
    }

    protected function createComponentUpdateForm()
    {
        $form = $this->createForm();

        $form->addHidden('id_verze');
        

        $form->addSubmit('send', 'Upravit');
        $form->onSuccess[] = $this->updateFormSubmitted;

        return $form;
    }

    public function updateFormSubmitted($form) 
    {
        $this->companies->update($form->values, $this->parseFormAdresses($form), $this->parseFormUsers($form), $this->getUser()->getId());
        $this->flashMessage("update");
        $this->redirect('Companies:default');
    }

    protected function createComponentDocumentsGrid($name)
    {
        return new DocumentsGrid($this, $name, $this->documents, $this->id_firma);
    }

    /**
     * Precteni hodnot adres z formulare
     * @param type $form
     * @return array
     */
    private function parseFormAdresses($form) 
    {
        $adresses = array();
            foreach ($form->httpData["adressesBox"] as $id => $value) {
                if ($id !== "ID" && $value["adresa"] !== "") {
                    array_push($adresses, $value);
            }
        }

        return $adresses;
    }

    /**
     * Precteni hodnot uzivatelu(kontaktu) z formulare
     * @param type $form
     * @return array
     */
    private function parseFormUsers($form) 
    {
        $users = array();
        foreach ($form->httpData["usersBox"] as $id => $value) {
            if ($id !== "ID" && $value["uzivatel"] !== "") {
                array_push($users, $value);
            }
        }

        return $users;
    }

    /**
     * Přidání formularovych prvku pro adresy vcetne hodnot
     * @param type $form
     * @param type $adresses
     */
    private function extendFormAdresses($form, $adresses) 
    {
        $counter = 0;
        $adressesBox = $form["adressesBox"];

        foreach ($adresses as $key => $adress) {
            $adressBox = $adressesBox->addContainer($counter);

            $adressBox->addTextArea("adresa", "Adresa:")->setDefaultValue($adress["adresa"]);
            $adressBox->addText("popis", "Popis:")->setDefaultValue($adress["popis"]);

            $counter++;
        }
    }

    /**
     * Přidání formularovych prvku pro uzivatele(kontakty) vcetne hodnot 
     * @param type $form
     * @param type $users
     */
    private function extendFormUsers($form, $users) 
    {
        $counter = 0;
        $usersBox = $form["usersBox"];
        foreach ($users as $key => $user) {
            //dump($user);
            $userBox = $usersBox->addContainer($counter);

            $userBox->addSelect("uzivatel", "Uživatel:", $this->users->getUsersSelectionList())
                    ->setDefaultValue($user["id_verze_uzivatele"]);
            $userBox->addText("popis", "Popis:")
                    ->setDefaultValue($user["popis"]);

            $counter++;
        }
    }

}
