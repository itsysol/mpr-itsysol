<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var \DK\Menu\UI\IControlFactory @inject */
    public $menuFactory;


    /**
     * @return \DK\Menu\UI\Control
     */
    protected function createComponentMenu()
    {
        return $this->menuFactory->create();
    }


    /**
     * Basically just helper for IDE because of @return annotation
     *
     * @return \DK\Menu\Menu
     */
    protected function getMenu()
    {
        return $this['menu']->getMenu();
    }
    
    public function handleSignOut() {
        $this->getUser()->logout();
        $this->redirect('Homepage:');
    }
    
}
