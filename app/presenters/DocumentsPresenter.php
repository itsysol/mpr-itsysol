<?php

namespace App\Presenters;

use Nette,
	App\Model,
	App\Model\Documents,
	Nette\Application\UI\Form,
	Nette\Http\Response,
	App\Controls\DocumentsGrid;

class DocumentsPresenter extends PrivatePresenter
{
	private $documents;
	private $companieId;
	private $edited;
	private $httpResponse;
	
	public function __construct(Documents $documents, Response $httpResponse)
	{
		$this->documents = $documents;
		$this->httpResponse = $httpResponse;
	}
	
	public function renderDefault()
	{
		
	}
	
	public function actionModify($id)
	{
		$this->edited = $this->documents->get($id);
    }

    /**
     * Komponanta formulare pridani dokumentu
     */
	protected function createComponentNewForm()
	{
		$form = new Form;
		$form->addHidden('id_firma');
		$form->addText('nazev', 'Název:')
                    ->setRequired('Zadejte název dokumentu!');
		$form->addSelect('typ','Typ dokumentu',$this->documents->typ)
                    ->setRequired('Zadejte typ dokumentu!');
        $form->addTextArea("popis", "Popis:");
        $form->addUpload('soubor','Dokument:')
                    ->setRequired('Vyberte dokument.');;
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->newFormSubmitted;
        return $form;
	}

    /**
     * Prijeti formulare pridani dokumentu
     * @param $form formular
     */
	public function newFormSubmitted($form) 
	{
		$this->documents->add($form->values, $this->getUser()->getId());
		$this->flashMessage("Dokument přidán!");
		$this->redirect('Companies:show',$form->values->id_firma);
	}

    /**
     * Obrazovka vlozeni noveho dokumentu
     * @param $id Databazovy identifikator firmy
     */	
    public function renderAdd($id) 
    {
		$this->companieId = $id;
        $form = $this->getComponent("newForm");
        $form->setDefaults(array('id_firma'=> $id));
    }
	
	protected function createComponentDocumentsGrid($name)
	{
		return new DocumentsGrid($this, $name, $this->documents);
	}

    /**
     * Komponenta formulare upravy dokumentu
     */
	protected function createComponentModifyForm() {
        $form = new Form;
        $form->addText('nazev', 'Název:')
                    ->setRequired('Zadejte název dokumentu!');
		$form->addSelect('typ','Typ dokumentu',$this->documents->typ)
                    ->setRequired('Zadejte typ dokumentu!');
        $form->addTextArea("popis", "Popis:");
        $form->addSubmit('send', 'Uložit');
        $form->setDefaults($this->edited->toArray());
        $form->onSuccess[] = $this->modifyFormSubmitted;
        return $form;
    }

    /**
     * KomponentPrijeti formulare upravy dokumentu
     * @param $form formular
     */    
    public function modifyFormSubmitted($form) {
        try {
            $this->documents->change($this->edited->id_dokument, $this->getUser()->id, $form->getValues());
            $this->flashMessage("Dokument upraven", "success");
            $this->redirect('Companies:show',$this->edited->id_firma);
        } catch (\Exception $e) {
            if ($e instanceof Nette\Application\AbortException) {
                throw $e;
            } else {
                $form->addError($e->getMessage());
            }
        }
    }
    
    /**
     * Smazani souboru
     */
    public function actionRemove($id)
	{
		$this->edited = $this->documents->get($id);
		$this->documents->remove($id);
		$this->redirect('Companies:show',$this->edited->id_firma);
    }

    /**
     * Stazeni jednoho souboru
     */
    public function actionDownload($id){
        $document = $this->documents->get($id);
        $url = $this->documents->getUrlByName($document->soubor);
        $this->httpResponse->redirect("../../".$url);
        exit(0);
        /*
        $this->httpResponse->setHeader("Content-Transfer-Encoding", "binary");
        $this->httpResponse->setHeader("Content-Description", "File Transfer");
        $this->httpResponse->setHeader("Content-Length", strlen($document->soubor));
        $this->httpResponse->setHeader('Content-Disposition', 'attachment; filename="'.$document->soubor.'"');
        $this->httpResponse->setContentType("application/download");
        readfile($this->documents->getUrlByName($document->soubor));
        */
    }
    
}
