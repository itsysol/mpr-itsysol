<?php

namespace App\Presenters;

use Nette,
	App\Model,
    App\Model\Users,
	App\Forms\UserFormFactory,
    Grido\Translations\FileTranslator,
    Grido\Components\Filters\Filter;


/**
 * Users presenter.
 */
class UsersPresenter extends PrivatePresenter
{
    private $users;
    /** @var UserFormFactory @inject */
    public $formFactory;
    public $activeId;
    public $edited;
    public $editedVersion;
    
    public function startup() {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Homepage:');
        }
    }
    
    public function __construct(Users $users) {
        $this->users = $users;
    }

	public function renderDefault()	{

	}

    /**
     * Priprav zobrazeni detailu
     */
    public function renderDetail($id) {
        $this->activeId = $id;
        $this->edited = $this->users->get($id);
        $this->editedVersion = $this->users->getCurrentVersion($id);
    }

    public function renderNew() {

    }

    /**
     * Prijeti uzivatelske akce - Zobrazeni historie
     * @param $id Id uzivatele
     */
    public function actionHistory($id) {
        $this->activeId = $id;
    }

    /**
     * Prijeti uzivatelske akce - Zmena dat uzivatele
     * @param $id Id uzivatele
     */
    public function actionModify($id) {
        $this->activeId = $id;
        $this->edited = $this->users->get($id);
        $this->editedVersion = $this->users->getCurrentVersion($id);
    }

    /**
     * Priprava zobrazeni modifikacni obrazovky
     * @param $id Id uzivatele
     */
    public function renderModify($id) {
        if (!$this->editedVersion) {
            $this->flashMessage("Neexistující uživatel", "error");
            $this->redirect('Users:');
        }
    }

    /**
     * Prijeti uzivatelske akce - Odstraneni uzivatele
     * @param $id Id uzivatele
     */
    public function actionRemove($id) {
        try {
            // TODO permissions check
            $this->users->hide($id);
            $this->flashMessage("Uživatel znepřístupněn", "success");
            $this->redirect('Users:');
        } catch (\Exception $e) {
            if ($e instanceof Nette\Application\AbortException) {
                throw $e;
            } else {
                $this->flashMessage($e->getMessage(), "error");
            }
        }
    }

    /**
     * Formular vytvoreni noveho uzivatele
     */
    protected function createComponentNewForm() {
        $form = $this->formFactory->create();
        $form->addSubmit('send', 'Vytvořit');
        $form->onSuccess[] = $this->newFormSubmitted;
        return $form;

    }

    /**
     * Formular noveho uzivatele prijat
     */
    public function newFormSubmitted($form) {
        try {
            list($created,$newpass) = $this->users->add($form->getValues(), $this->getUser()->id);
            if (!empty($newpass)) {
                $this->flashMessage("Nové heslo je \"" . $newpass . "\"", "info");
            }
            $this->flashMessage("Nový uživatel vytvořen", "success");
            $this->redirect('Users:detail', array("id" => $created->id_uzivatel));
        } catch (\Exception $e) {
            if ($e instanceof Nette\Application\AbortException) {
                throw $e;
            } else {
                $form->addError($e->getMessage());
            }
        }
    }

    /**
     * Formular zmeny dat uzivatele
     */
    protected function createComponentModifyForm() {
        $form = $this->formFactory->create();
        $form->setDefaults(array(
            'firstname' => $this->editedVersion->jmeno,
            'lastname' => $this->editedVersion->prijmeni,
            'degree' => $this->editedVersion->titul,
            'username' => $this->edited->login,
            'phone' => $this->editedVersion->telefon,
            'email' => $this->editedVersion->email,
            'role' => $this->editedVersion->role,
        ));
        $form->addSubmit('send', 'Uložit');
        $form->onSuccess[] = $this->modifyFormSubmitted;
        return $form;
    }

    /**
     * Formular zmeny uzivatele prijat
     */
    public function modifyFormSubmitted($form) {
        try {
            $this->users->change($this->activeId, $this->getUser()->id, $form->getValues());
            $this->flashMessage("Uživatel upraven", "success");
            $this->redirect('Users:');
        } catch (\Exception $e) {
            if ($e instanceof Nette\Application\AbortException) {
                throw $e;
            } else {
                $form->addError($e->getMessage());
            }
        }
    }

    protected function createComponentUsersGrid($name) {
        $grid = new \Grido\Grid($this, $name);
        $grid->setModel($this->users->getVersionTable()->where('soucasna'));
        $grid->setTranslator(new FileTranslator('cs'));
        $grid->setDefaultSort(array('prijmeni' => 'ASC'));
        
        $grid->addFilterText('jmeno', 'Jméno');
        $grid->addFilterText('prijmeni', 'Příjmení');
        
        
        $grid->addColumnText('jmeno', 'Jméno')->setSortable();
        $grid->addColumnText('prijmeni', 'Příjmění')->setSortable();
        
        $grid->addActionHref('detail', 'Detail');
        $grid->addActionHref('history', 'Historie');
        $grid->addActionHref('modify', 'Upravit');
        $grid->addActionHref('remove', 'Odstranit');
        return $grid;
    }


    protected function createComponentHistoryGrid($name) {
        $grid = new \Grido\Grid($this, $name);
        $grid->setModel($this->users->getVersionTable()->where('id_uzivatel', $this->activeId));
        $grid->setTranslator(new FileTranslator('cs'));
        $grid->setDefaultSort(array('zmeneno' => 'DESC'));     
        $grid->setFilterRenderType(Filter::RENDER_INNER);
        $grid->addColumnText('zmeneno', 'Datum')->setSortable();
        $grid->addColumnText('jmeno', 'Jméno')->setSortable();
        $grid->addColumnText('prijmeni', 'Příjmění')->setSortable();
        
        $grid->addActionHref('detail', 'Detail');
        return $grid;
    }
}
