<?php

namespace App\Presenters;

use Nette,
	App\Model;


abstract class PrivatePresenter extends BasePresenter
{
    /* Over ze je uzivatel prihlaseny */
    public function startup() {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Homepage:');
        }
    }
}
