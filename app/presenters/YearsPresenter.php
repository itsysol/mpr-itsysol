<?php

namespace App\Presenters;

use Nette,
	App\Model,
	App\Model\Years,
	App\Model\Companies,
	App\Model\Users,
	App\Forms\YearFormFactory,
	Grido\Translations\FileTranslator,
	Grido\Components\Filters\Filter,
	Nette\Database\Table\Selection;

class YearsPresenter extends PrivatePresenter
{
	private $years;
	private $companies;
	private $users;
    /** @var YearFormFactory @inject */
    public $formFactory;
	
	public function __construct(Years $years, Companies $companies, Users $users)
	{
		$this->years = $years;
		$this->companies = $companies;
		$this->users = $users;
	}

    /**
     * Vykresli tabulku rocniku
     */
	public function renderDefault()
	{
		//nacteni rocniku
		$years = $this->years->getYears();
		$out = false;
		//nacteni firem pro rocniky
		foreach($years as $y)
		{
			$outLine['rok'] = $y['rok'];
			$comp = $this->companies->getCompaniesForYear($y['id_rocnik']);
			$outLine['company'] = $comp->fetchAll();
			if (count($outLine['company']) == 0)
				$outLine['company'] = false;
			$out[] = $outLine;
		}
		$this->template->years = $out;

		//$out = $this->companies->getCompaniesForYears();
		//$this->template->years = $out;
	}

    /**
     * Komponenta seznamu rocniku
     */
	protected function createComponentYearsGrid($name)
	{
		$grid = new \Grido\Grid($this, $name);
		$grid->setPrimaryKey("id_rocnik");
		$grid->setModel($this->years->getTable());
		$grid->setTranslator(new FileTranslator('cs'));
		$grid->setFilterRenderType(Filter::RENDER_INNER);
		$grid->setDefaultSort(array('rok' => 'ASC'));
		
		$grid->addColumnText('rok', 'Rok')->setSortable();
		
		//$grid->addActionHref('edit', 'Editace');
		//$grid->addActionHref('remove', 'Odstranit');
		return $grid;
	}
	
    /**
     * Komponenta vytvoreni rocniku
     */
	protected function createComponentNewYearForm() {
        $form = $this->formFactory->create();
        $form->addSubmit('send', 'Vytvořit');
        $form->onSuccess[] = $this->newFormSubmitted;
        return $form;
    }

    /**
     * Snaha o vytvoreni rocniku
     * @param $form Formular noveho rocniku
     */
    public function newFormSubmitted($form) {
        try {
            $this->years->add($form->getValues());
            $this->flashMessage("Nový ročník vytvořen", "success");
            $this->redirect('Years:default');
        } catch (\Exception $e) {
            if ($e instanceof Nette\Application\AbortException) {
                throw $e;
            } else {
                $form->addError($e->getMessage());
            }
        }
    }
}
